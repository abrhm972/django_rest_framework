from datetime import datetime
from django.db import models

# Create your models here.
class Vehiculo(models.Model):
    dt_string = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    placa = models.CharField(max_length=10)
    alias = models.CharField(max_length=10)
    fecha_creacion = models.DateTimeField(default=dt_string, blank=True)
    fecha_edicion = models.DateTimeField(auto_now=dt_string, blank=True)
    compania = models.CharField(max_length=255)
    tipo_vehiculo = models.CharField(max_length=255, default='')
    id = models.AutoField(primary_key=True)
