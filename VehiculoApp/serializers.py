from rest_framework import serializers
from VehiculoApp.models import Vehiculo

class VehiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Vehiculo
        fields=('placa', 'alias', 'fecha_creacion', 'fecha_edicion', 'compania', 'tipo_vehiculo', 'id')



class TestVehiculoSerializer(serializers.Serializer):
    placa = serializers.CharField(max_length = 10)
    alias = serializers.CharField(max_length = 10)
    compania = serializers.CharField(max_length = 255)
    tipo_vehiculo = serializers.CharField(max_length = 255, default='')

    def validate_placa(self, value):
        delValue = value.replace('-', '')
        delValue = delValue.replace('_', '')
        if value == "":
            raise serializers.ValidationError('El campo de placa no puede estar en blanco.')
        elif not "-" in value and not "_" in value:

            raise serializers.ValidationError('debe de tener este símbolo - o _')

        elif delValue.isalnum() and not delValue.isalpha() and not delValue.isdigit():
            return value
        raise serializers.ValidationError('debe contener letras y números')

    def validate_alias(self, value):
        delValue = value.replace('-', '')
        delValue = delValue.replace('_', '')

        if not "-" in value and not "_" in value:
            raise serializers.ValidationError('debe de tener este símbolo - o _')
        elif delValue.isalnum() and not delValue.isalpha() and not delValue.isdigit():
            return value
        raise serializers.ValidationError('debe contener letras y números')

    def validate(self, data):
        return data


    def create(self, validated_data):
        return Vehiculo.objects.create(**validated_data)


    def update(self, instance, validated_data):
        instance.id = validated_data.get('id', instance.id)
        instance.save()
        return instance