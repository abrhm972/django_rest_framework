from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from VehiculoApp.models import Vehiculo
from VehiculoApp.serializers import VehiculoSerializer, TestVehiculoSerializer






@api_view(['GET'])
def login(request):
  
    user = User.objects.get(username='admin')
    
    token = Token.objects.create(user=user)
    return Response({'status': 'Ok', 'Token': token.key}, status=status.HTTP_200_OK)





@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def vehiculoApi(request):

    # LISTAR / CREAR
    if request.method == 'GET':
        vehiculo = Vehiculo.objects.all()
        vehiculo_serializer = VehiculoSerializer(vehiculo, many=True)
        arr=[]
        for i in vehiculo_serializer.data:
            raw = list(i.values())
            arr.append({
                "placa": raw[0],
                "alias": raw[1],
                "fecha_creacion": (raw[2][:-6]).replace('T', ' '),
                "fecha_edicion": (raw[3][:-13]).replace('T', ' '),
                "compania": raw[4],
                "tipo_vehiculo": raw[5],
                "id": raw[6]
            })
            print(raw)
        return Response(arr, status=status.HTTP_200_OK)


    elif request.method == 'POST':
        vehiculo_data = JSONParser().parse(request)
        vehiculo_serializer = TestVehiculoSerializer(data=vehiculo_data)
        if vehiculo_serializer.is_valid():
            vehiculo_serializer.save()
            rpta = {
                'status': 'Created',
                'data': vehiculo_serializer.data,
                'message': 'Creación éxitosa'
            }
            return Response(rpta, status = status.HTTP_201_CREATED)
    

        campo = list(vehiculo_serializer.errors.keys())[0]
        error = list(vehiculo_serializer.errors.values())[0][0]
        return Response({'status': 'Error', 'message': campo + ' '+error}, status = status.HTTP_400_BAD_REQUEST)


    # OBTENER DATOS POR ID
    
    vehiculo_data = JSONParser().parse(request)
    vehiculo = Vehiculo.objects.filter(id = vehiculo_data['id']).first()
   


    if vehiculo:
        # ACTUALIZAR / ELIMINAR
        if request.method == 'PUT':
            vehiculo_serializer = TestVehiculoSerializer(vehiculo, data = vehiculo_data)
            if vehiculo_serializer.is_valid():
                vehiculo_serializer.save()
                rpta = {
                'status': 'Ok',
                'data': vehiculo_serializer.data,
                'message': 'Actualizado con éxito'
                }
                return Response(rpta, status = status.HTTP_200_OK)
            campo = list(vehiculo_serializer.errors.keys())[0]
            error = list(vehiculo_serializer.errors.values())[0][0]
            return Response({'status': 'Error', 'message': campo + ' ' + error}, status = status.HTTP_400_BAD_REQUEST)


        elif request.method == 'DELETE':
            vehiculo.delete()
            rpta = {
                'status': 'Ok',
                'message': 'Eliminación éxitosa'
            }
            return Response(rpta, status = status.HTTP_200_OK)
    return Response({'status': 'Error', 'message': 'El id no éxiste'}, status = status.HTTP_400_BAD_REQUEST)


