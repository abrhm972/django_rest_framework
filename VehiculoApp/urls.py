from django.urls import path
from VehiculoApp.views import vehiculoApi, login


urlpatterns  = [
    path('vehiculo', vehiculoApi),
]

urlpatterns += path('login', login),